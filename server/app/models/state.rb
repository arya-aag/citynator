# frozen_string_literal: true

# State
class State < ApplicationRecord
    validates :name, presence: true
    has_many :cities
  end
  