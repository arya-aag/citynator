# frozen_string_literal: true

# City
class City < ApplicationRecord
  validates :name, :state_id, presence: true
  belongs_to :state
end
