# frozen_string_literal: true

# AddForeignKeyToCity
class AddForeignKeyToCity < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :cities, :states
  end
end
