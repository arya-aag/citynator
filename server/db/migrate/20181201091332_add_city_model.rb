# frozen_string_literal: true

# AddCityModel
class AddCityModel < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :state

      t.timestamps
    end
  end
end
