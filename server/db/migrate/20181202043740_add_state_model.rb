# frozen_string_literal: true

# AddStateModel
class AddStateModel < ActiveRecord::Migration[5.2]
  def change
    create_table :states do |t|
      t.string :name
      t.integer :cities_count, default: 0

      t.timestamps
    end
  end
end
