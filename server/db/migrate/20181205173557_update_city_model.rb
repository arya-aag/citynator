# frozen_string_literal: true

# UpdateCityModel
class UpdateCityModel < ActiveRecord::Migration[5.2]
  def change
    remove_column :cities, :state, :string
    add_column :cities, :state_id, :bigint, foreign_key: true
  end
end
